<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/login','AuthController@login')->name('login');

Route::get('/logout', 'AuthController@logout')->middleware('auth:api');


Route::get('/globals_classes', function() {  
    $base_classes = DB::table('base_classes')->get();
    $base_streams = DB::table('base_streams')->get();
    
    return ['base_classes'=>$base_classes,'base_streams'=>$base_streams];
});


Route::resource('student', 'StudentsController');
Route::resource('subject', 'SubjectController');
Route::resource('teacher', 'TeachersController');


