import Vue from 'vue';
import Vuex from 'vuex';
import router from '../router/';
import globals from './modules/globals'

Vue.use(Vuex);

export default new Vuex.Store({
    state: { //data
        scl_token: localStorage.getItem('SCL_token') || '',
    },
    getters: { //computed properties
        isAuthenticated: state => !!state.scl_token,
    },
    mutations: { //change the state
        SET_TOKEN(state, token) {
            state.scl_token = token
        },
        UNSET_TOKEN(state) {
            state.scl_token = '';
        }
    },
    actions: { //methods
        set_token({
            commit
        }, token) {
            localStorage.setItem('SCL_token', token);
            commit('SET_TOKEN', token)
        },
        unset_token({
            commit
        }) {
            localStorage.removeItem('SCL_token');
            commit('UNSET_TOKEN')
        },
        logout(context) {
            context.dispatch('unset_token')
            console.log('logging out...')
            router.push('Login');
        },
    },
    modules:{
        globals,
    }

})
