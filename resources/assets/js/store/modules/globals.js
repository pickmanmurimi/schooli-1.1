const globals = {
    namespaced:true,
    state:{
        base_classes:[],
        base_streams:[],
    },
    mutations:{
        SET_BASE_CLASSES(state,base_classes){
            // base_classes.forEach(element => {
            //     base_classes = ['name']
            // });
            state.base_classes = base_classes
        },
        SET_BASE_STREAMS(state,base_streams){
            state.base_streams = base_streams
        }
    },
    actions:{
        get_classes_globals({commit}){
            axios.get('api/globals_classes')
            .then(resp => {
                commit('SET_BASE_CLASSES',resp.data.base_classes)
                commit('SET_BASE_STREAMS',resp.data.base_streams)
            })
            .catch(err => console.log(err))
        }
    }
}

export default globals