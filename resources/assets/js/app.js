
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
import Vue from 'vue';
import Vuetify from 'vuetify'
import router from './router/';
import swal from 'sweetalert';
import store from './store/';
import 'vuetify/dist/vuetify.min.css';
import 'material-design-icons-iconfont/dist/material-design-icons.css'
// axios intercepter
require('./helpers/axios.js');

Vue.use(Vuetify, { theme: { primary: "#00897B", secondary: "#BDBDBD", accent: "#E91E63", } });

// some global components
Vue.component('app-header', require('./components/Header.vue'));

const app = new Vue({
    el: '#app',
    router,
    store
});
