import store from '../store/index'
import swal from 'sweetalert';
// Add a request interceptor
axios.interceptors.request.use(function (config) {
    // Do something before request is sent
    config.headers.Authorization = `Bearer ` + store.state.scl_token;
    console.log('before request is sent...');
    return config;
}, function (error) {
    // Do something with 
    console.log('request error...');
    return Promise.reject(error);
});

// Add a response interceptor
axios.interceptors.response.use(function (response) {
    // Do something with response data
    console.log('after request is sent and data returned...');
    return response;
}, function (error) {
    // Do something with response error
    if (!error.response) {
        swal('Network Error :(', 'Kindly check your connection', 'error')
        return Promise.reject(error);
    }
    switch (error.response.status) {
        case 401:
            store.dispatch('logout')
            return Promise.reject(error);
            break;
        case 500:
            swal('Server Error :(', 'Kindly contact your web master', 'error')
            return Promise.reject(error);
            break;
        case 422:
            break;
        default:
            swal('Error :(', 'Kindly contact your web master', 'error')

    }
    return Promise.reject(error);
});
