import Vue from 'vue'
import VueRouter from 'vue-router'
import store from '../store/index'
Vue.use(VueRouter);

// check for authentication
const ifAuthenticated = (to, from, next) => {
    if (store.getters.isAuthenticated) {
      next()
      return
    }
    next('/Login')
  }
const ifNotAuthenticated = (to, from, next) => {
    if (!store.getters.isAuthenticated) {
      next()
      return
    }
    next('/Dashboard')
  }

let Home = require('../components/Home.vue');
let Dashboard = require('../components/Dashboard.vue');
let Login = require('../components/Login.vue');
let Admission = require('../components/Admission.vue')
let StudentList = require('../components/StudentList.vue');
let Subjects = require('../components/Subjects.vue')
let Exams = require('../components/Exams.vue')
let Backups = require('../components/Backups.vue')
let Balances = require('../components/Balances.vue')
let Fee = require('../components/Fee.vue')
let Analysis = require('../components/Analysis.vue')
let Promotion = require('../components/Promotion.vue')
let TeachingStaff = require('../components/TeachingStaff.vue')
let NonTeachingStaff = require('../components/NonTeachingStaff.vue')

let SettingsGeneral = require('../components/SettingsGeneral')

const routes = [
    { path: '/', component:Home,beforeEnter: ifNotAuthenticated},
    { path: '/Home', component:Home,beforeEnter: ifNotAuthenticated},
    { path: '/Dashboard', component:Dashboard,beforeEnter: ifAuthenticated},
    { path: '/Login', component:Login,beforeEnter: ifNotAuthenticated},
    { path: '/Admission', component:Admission,beforeEnter: ifAuthenticated},
    { path: '/StudentList', component:StudentList,beforeEnter: ifAuthenticated},
    { path: '/Subjects', component:Subjects,beforeEnter: ifAuthenticated},
    { path: '/Exams', component:Exams,beforeEnter: ifAuthenticated},
    { path: '/Backups', component:Backups,beforeEnter: ifAuthenticated},
    { path: '/NonTeachingStaff', component:NonTeachingStaff,beforeEnter: ifAuthenticated},
    { path: '/TeachingStaff', component:TeachingStaff,beforeEnter: ifAuthenticated},
    { path: '/Balances', component:Balances,beforeEnter: ifAuthenticated},
    { path: '/Promotion', component:Promotion,beforeEnter: ifAuthenticated},
    { path: '/Fee', component:Fee,beforeEnter: ifAuthenticated},
    { path: '/Analysis', component:Analysis,beforeEnter: ifAuthenticated},
    { path: '/SettingsGeneral', component:SettingsGeneral,beforeEnter: ifAuthenticated},
]

export default new VueRouter({
    // mode:'history',
    routes,
});