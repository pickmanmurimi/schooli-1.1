<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\BaseClass;
use App\BaseStream;

class Stream extends Model
{
    //lets get the base class for each stream
    public function baseClass()
    {
    	return $this->belongsTo('App\BaseClass','base_classes_id','id');
    }

    // lets get the base stream for each stream
    public function baseStream()
    {
    	return $this->belongsTo('App\BaseStream','base_streams_id','id');
    }
    // students in the class
    public function Students()
    {
    	return $this->belongsToMany('App\Student','stream_student');
    }
    public static function studentList()
    {
   		return DB::table('current student list')->get();
   		
		/*select students.id,students.first_name,streams.id AS streamId,base_classes.name AS className,base_streams.name AS streamName
		FROM students LEFT JOIN stream_student ON
		students.id = stream_student.student_id 
		INNER JOIN streams ON
		stream_student.stream_id = streams.id
		INNER JOIN base_classes ON
		streams.base_classes_id = base_classes.id
		INNER JOIN base_streams ON
		streams.base_streams_id = base_streams.id
		WHERE stream_student.academic_year_id = 1*/
    }
    /*-----------------------------
        the eloquet relationship*/
    // students in the current classlist
    public function currentClasslist()
    {
        return $this->hasMany('App\currentStudentList','stream_id');
    }
}
