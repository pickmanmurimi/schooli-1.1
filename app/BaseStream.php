<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Stream;

class BaseStream extends Model
{
	protected $table = 'scl_base_streams';
    
    //lets get the streams that each baseStream has
   	public function Streams()
   	{
   		return $this->hasMany('App\Stream','base_streams_id');
   	}
   	// students in the current classlist
  	public function currentClasslist()
  	{
    	return $this->hasMany('App\currentStudentList','base_streams_id');
  	}
}
