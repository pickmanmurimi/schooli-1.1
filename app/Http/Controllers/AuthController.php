<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    // login
    public function login(Request $request){
        
        $this->validate($request,[
            'email' => 'required|email',
            'password' => 'required',
        ]);

        $creditials = ['email' => $request->email, 'password' => $request->password];
        
        if (Auth::attempt($creditials)){
            $user = Auth::user();
            $token = $user->createToken('schooli')->accessToken;
        
            return response()->json(['token' => $token, 'message' => 'You are logged In'],200);    
        }else{

            return response()->json(['message' => 'Wrong Email and Password combination'],401);
        }
        
    }
    
    // logout
    public function logout(Request $request){
            $request->user()->token()->revoke();
            return response()->json(['message' => $request->user()->name . ' You are logged out'],200);
    }
}