<?php

namespace App\Http\Controllers;

use Validator;
use App\subject;
use Illuminate\Http\Request;

class SubjectController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $subjects = Subject::orderBy('code')->get();
        return response()->json($subjects,200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
               
        // prevent duplication
        Validator::extend('uniqueCode', function ($attribute, $value, $parameters, $validator) {
            $count = subject::where('code', $value)
                                        ->where('abbreviation', $parameters[0])
                                        ->count();

            return $count === 0;
            });
        $messages =['unique_code' => 'The the abbreviation and subject code combination already exists',];
        // validation
        Validator::make($request->all(),[
                'name' => 'required|unique:subjects',
                'abbreviation' => 'required|max:8',
                'subject_code' => 'required|integer|uniqueCode:'.$request->abbreviation,
                ],$messages)->validate();

        $subject = new subject();
        $subject->name          = $request->name;
        $subject->abbreviation  = $request->abbreviation;
        $subject->code          = $request->subject_code;
        $subject->group         = $request->group;
        $subject->compulsory    = (int) (bool) $request->compulsory;

        // save to db
        $subject->save();
        
        // $subjects = Subject::all();
        return response()->json($subject,200);
                
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\subject  $subject
     * @return \Illuminate\Http\Response
     */
    public function show(subject $subject)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\subject  $subject
     * @return \Illuminate\Http\Response
     */
    public function edit(subject $subject)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\subject  $subject
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, subject $subject)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\subject  $subject
     * @return \Illuminate\Http\Response
     */
    public function destroy(subject $subject)
    {
        //delete the subject
        return response()->json(Subject::find($subject->id)->delete(),200);
    }
}
