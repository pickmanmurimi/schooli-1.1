<?php

namespace App\Http\Controllers;

use Validator;
use App\Student;
use App\Guardian;
use App\stream;
use App\Http\Requests\StoreStudent;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Request as Rq;

class StudentsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {

        $students = Student::StudentList();
        //Comment the above line and uncomment the line below to enable,server side pagination
        // $students = DB::table('current student list')->paginate(30);
        return response()->json($students,200);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        // return view('students.admission');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreStudent $request)
    {
        //
        // return $request->all();
        $validated_rq = $request->validated();
        $student = new Student();
                $student->first_name = strtoupper($request->first_name);
                $student->last_name = strtoupper($request->last_name);
                $student->Surname = strtoupper($request->Surname);
                $student->gender = $request->gender;
                $student->regNo = $request->regNo;
                $student->date_of_birth = $request->date_of_birth;
                $student->birth_cert_number = $request->birth_cert_number;
                $student->physical_address = strtoupper($request->physical_address);
                $student->previous_school = strtoupper($request->previous_school);
                $student->entry_score = $request->entry_score;
                $student->entry_grade = $request->entry_grade;

            if ($request->reason_for_transfer == "Other") {
                $request->validate([
                    'specify_reason_for_transfer' => 'required',
                ]);
                $student->reason_for_transfer = $request->specify_reason_for_transfer;
            }else{ $student->reason_for_transfer = $request->reason_for_transfer;}

        // save the student in the db
        $student->save();

        // get the stream defined by the class stream combination
        $stream = Stream::where('base_classes_id',$request->class)->where('base_streams_id',$request->stream)->get();
        
        // save the students class
        $student->streams()->attach($stream->first()->id,[
                                            'academic_year_id' => DB::select('select id from scl_academic_years where status = ?', ['active'])[0]->id,
                                            'created_at' => date('Y-m-d H:m:s'),
                                            'updated_at' => date('Y-m-d H:m:s')
                                                    ]);
        
        // create a new guardian
        $Guardian = new Guardian();
                $Guardian->guardian_full_name = strtoupper($request->guardian_full_name);
                $Guardian->phone_number = $request->phone_number;
                // $Guardian->p_email = $request->p_email;

        // save the guardian in the db
                $student->Guardian()->save($Guardian);
        // $Guardian->save();

        //return Student::all();
        return response()->json($student,200);
        
    }

    /**
     * Display the specified resource.
     */
    public function show(Student $student)
    {
        $student = Student::with('Guardian')->find($student->id); 
        return response()->json($student,200);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Student $student)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Student $student)
    {
        $data = $request->data;
    //   $removeKeys = ['fullName','status','surname','stream_id','className','streamName','id','guardiansId','first_name','last_name','guardian_full_name'];
    //     foreach($removeKeys as $key) {
    //         unset($data[$key]);
    //     }  
        DB::table('current student list')
        ->where('id', $student->id)
        ->update([
            'guardian_full_name' => $data['guardian_full_name'],    
            'entry_score' => $data['entry_score']
            ]);
            
        return response()->json($data,200);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Student $student)
    {
        //
    }

    public function parents()
    {
        // //
        // if(Rq::ajax())
        // {
        // return view('students.parents')->renderSections()['content'];
        // }
        // return view('students.parents');
    }
}
