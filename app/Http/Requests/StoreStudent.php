<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreStudent extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return True;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
                    "first_name" => "required|max:50",
                    "last_name" => "required|max:50",
                    "Surname" => "max:50",
                    "gender" => "required",
                    "regNo" => "required|unique:students|max:20",
                    "date_of_birth" => "required|date:'d-m-Y H:i:s.u'",
                    "birth_cert_number" => "nullable|unique:students,birth_cert_number,NULL,id,birth_cert_number,NOT_NULL",
                    "class" => "required|exists:streams,base_classes_id",
                    "stream" => "required|exists:streams,base_streams_id",
                    "residence"=> "max:50",
                    "previous_school" => "max:70",
                    "reason_for_transfer"=> "required",
                    "specify_reason_for_transfer"=> "max:100",
                    "entry_score"=> "nullable|max:5|digits_between:1,5",
                    "entry_grade"=> "max:2|nullable",
                    "guardian_full_name"=> "required|max:100",
                    "phone_number"=> "required|max:15|digits_between:9,15",
        ];
    }
}
