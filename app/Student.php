<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Guardian;
use Illuminate\Support\Facades\DB;

class Student extends Model
{
    //
   	public function Guardian()
   	{
   		/*student belongs to one parent/guardian*/

   		return $this->hasOne(Guardian::class);
   	}
   	// streams the student belongs to
   	public function Streams()
   	{
   		return $this->belongsToMany('App\Stream','stream_student');
   	}
   	// students list
   	// this function only returns the students who have been assigned a class
   	public static function studentList()
   	{
   		return DB::table('current student list')->get();
   	}
}
