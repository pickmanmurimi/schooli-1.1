<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Stream;
use App\BaseStreams;
use Illuminate\Support\Facades\DB;

class BaseClass extends Model
{
	protected $table = 'scl_base_classes';
    
    //lets get the streams that each base class has
   	public function Streams()
   	{
   		return $this->hasMany('App\Stream','base_classes_id');
   	}

   	// lets get the basestreams that each base class has
   	// note that this is a distant relation,
   	// we do not have a table that links BaseStreams and BaseClasses directly
   	public function BaseStreams()
   	{
   		return  DB::select('
   		select `base_streams`.*,
		`streams`.`base_classes_id`
		 from 
		`base_streams`
		 inner join 
		`streams`
		 on 
		`streams`.`base_streams_id` = `base_streams`.`id` 
		where `streams`.`base_classes_id` =?',[$this->id]);
   	}
      // students in the current classlist
      public function currentClasslist()
      {
         return $this->hasMany('App\currentStudentList','base_classes_id');
      }
}
