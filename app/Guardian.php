<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Student;

class Guardian extends Model
{
    //
    public function Student()
    {
    	/*one parent has many students*/
   		return $this->belongsTo('App\Student');
    }
}
