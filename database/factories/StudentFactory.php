<?php

use Faker\Generator as Faker;

$factory->define(App\Student::class, function (Faker $faker) {
    return [
        'regNo' => rand(100,2000),
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'surname' => $faker->lastName,
        'gender' => $faker->numberBetween(1,2),
        'date_of_birth' => $faker->dateTimeBetween($startDate = '-17 years', $endDate = '-12 years'),
        'birth_cert_number' => $faker->isbn10,
        'physical_address' => $faker->word,
        'prevIous_school' => $faker->company,
        'reason_for_transfer'=>'Graduated',
        'entry_score' => $faker->numberBetween(300,450),
    ];
});

// regNo	varchar(20)	 
// first_name	varchar(191)	 
// last_name	varchar(191)	 
// surname	varchar(191) NULL	 
// gender	enum('male','female','other')	 
// date_of_birth	varchar(191)	 
// birth_cert_number	timestamp NULL	 
// profile_picture	varchar(191) NULL	 
// physical_address	varchar(191) NULL	 
// previous_school	varchar(191) NULL	 
// reason_for_transfer	varchar(191) NULL	 
// entry_score	int(11) NULL	 
// entry_grade