<?php

use Illuminate\Database\Seeder;

class GuardiansTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        for($students = 1; $students <= 1500; $students++){
            DB::table('Guardians')->insert([
                    'student_id' => $students,
                    'guardian_full_name' => $faker->name,
                    'phone_number' => $faker->phoneNumber,
                    'p_email' => $faker->email,
                ]);
        }
    }
}

// id	bigint(20) unsigned Auto Increment	 
// student_id	bigint(20) unsigned	 
// guardian_full_name	varchar(191) NULL	 
// phone_number	bigint(15) NULL	 
// p_email	varchar(191) NULL	 
// created_at	timestamp NULL	 
// updated_at