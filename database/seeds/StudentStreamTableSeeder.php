<?php

use Illuminate\Database\Seeder;

class StudentStreamTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        for($students = 1; $students <= 1500; $students++){
            DB::table('stream_student')->insert([
                    'stream_id' => $faker->numberBetween(1,8),
                    'student_id' => $students,
                    'academic_year_id' => 1,
                ]);
        }
    }
}

// id	int(10) unsigned Auto Increment	 
// stream_id	int(11)	 
// student_id	int(11)	 
// academic_year_id	int(11) NULL	 
// created_at	timestamp NULL	 
// updated_at	timestamp NULL	 
