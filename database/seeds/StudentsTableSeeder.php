<?php
use Illuminate\ Database\ Seeder;

class StudentsTableSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public

    function run() {
        $faker = Faker\Factory::create();
        for($students = 0; $students < 1500; $students++){
            DB::table('students')->insert([
                    'regNo' => 600+$students,
                    'first_name' => $faker->firstName,
                    'last_name' => $faker->lastName,
                    'surname' => $faker->lastName,
                    'gender' => $faker->numberBetween(1, 2),
                    'date_of_birth' => $faker->dateTimeBetween($startDate = '-17 years', $endDate = '-12 years'),
                    'birth_cert_number' => $faker->isbn10,
                    'physical_address' => $faker->word,
                    'prevIous_school' => $faker->company,
                    'reason_for_transfer' => 'Graduated',
                    'entry_score' => $faker->numberBetween(300, 450),
                ]);
        }

    }
}
